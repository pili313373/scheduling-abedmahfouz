<?php
namespace AbedMahfouz\Scheduling;

class ProfRelation {

    public $sectionCourse;
    public $prof_id;

    /**
     * @param  \AbedMahfouz\Scheduling\SectionCourse  $sectionCourse
     * @param  int $prof_id
     */
    public function __construct(\AbedMahfouz\Scheduling\SectionCourse $sectionCourse, $prof_id) {
        $this->sectionCourse = $sectionCourse;
        $this->prof_id = intval($prof_id);
    }

}
