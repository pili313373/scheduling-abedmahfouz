<?php
namespace AbedMahfouz\Scheduling;

class Helper {

    /**
     * generate section-course array key
     *
     * @param  int $section_id
     * @param  int $course_id
     * @return string
     */
    public static function makeSectionCourseKey($section_id, $course_id) {
        return $section_id.'/'.$course_id;
    }

    /**
     * generate schedule array key
     *
     * @param  int $section_id
     * @param  int $day_id
     * @param  int $session_id
     * @return string
     */
    public static function makeScheduleKey($section_id, $day_id, $session_id) {
        return $section_id.'/'.$day_id.'/'.$session_id;
    }

}
