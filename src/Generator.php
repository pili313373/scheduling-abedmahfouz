<?php
namespace AbedMahfouz\Scheduling;

use \AbedMahfouz\Scheduling\Helper as Helper;

class Generator {

    public $sections;
    public $sectionCourses;
    public $profRelations;
    public $profIDs;
    public $daysSessions;
    public $maxSessionPerDay;

    public $profsSchedule = [];
    public $dayIDs = [];
    public $sessionIDs = [];

    public $warnings = [];
    public $errors = [];

    public $schedule = [];

    /**
     * @param  array of \AbedMahfouz\Scheduling\Section  $sections
     * @param  array of \AbedMahfouz\Scheduling\SectionCourse  $sectionCourses
     * @param  array of \AbedMahfouz\Scheduling\ProfRelation  $profRelations
     * @param  array of prof id $profIDs
     * @param  array 2D $daysSessions days[sessions[]]
     * @param  int $maxSessionPerDay
     * @return void
     */
    public function __construct($sections, $sectionCourses, $profRelations, $profIDs =[], $daysSessions, $maxSessionPerDay) {
        $this->sections = $sections;
        $this->sectionCourses = $sectionCourses;
        $this->profRelations = $profRelations;
        $this->profIDs = $profIDs;
        $this->daysSessions = $daysSessions;
        $this->maxSessionPerDay = $maxSessionPerDay;
    }



    /**
     * generate schedule array key
     *
     * @param  int $section_id
     * @param  int $day_id
     * @param  int $session_id
     * @return string
     */
    // public static function makeScheduleKey($section_id, $day_id, $session_id) {
    // -------------------------------------------------------------------------


    public function generate() {
        // empty first
        $this->clearSchedule();
        $this->clearMessages();

        // reset profs schedule
        $this->resetProfSchedule();

        // reset day IDs and session IDs
        $this->resetDaySessionIDs();




        foreach ($this->sections as $section) {
            foreach ($this->sectionCourses as $sc){
                if ($sc->section !== $section) continue;

                $profIDs = [];
                foreach($this->profRelations as $pr) {
                    if ($pr->sectionCourse === $sc) {
                        $profIDs[] = $pr->prof_id;
                    }
                }


                if (count($profIDs) == 0) {
                    $this->warnings[] = "There is no Prof assigned in {$sc}";
                    continue;
                }


                // ...
                // now we have $section, $sectionCourse, and $profs
                // try to add to schedule now
                // ...

                // for ($hoursLeft = min($sc->course_hours, 5); $hoursLeft > 0; $hoursLeft--) {
                for ($hoursLeft = 3; $hoursLeft > 0; $hoursLeft--) {

                    $maxTry = 1000 * 10;

                    // if only 2 or less hour left for this course, then we just need to try to add
                    // if ($hoursLeft < 3) {
                        $result = $this->tryAddOneHour($sc, $profIDs, $maxTry);
                        if ($result == false) {
                            $this->errors[] = "{$sc} cant be assigned!";
                            break;
                        }
                        continue;
                    // }

                    // if more than 1 hour left for this course,
                    // then we try add 2 hours, if still can't then 1 hour

                    $hoursLeft--;
                    $result = $this->tryAddTwoHour($sc, $profIDs, $maxTry);

                    if (!$result) {
                        $hoursLeft++;
                        $result = $this->tryAddOneHour($sc, $profIDs, $maxTry);

                        // if 2 hours cant be added, 1 hours cant be added, then its error
                        if ($result == false) {
                            $this->errors[] = "{$sc} cant be assigned!";
                            break;
                        }
                    }
                }
            }
        }

        return $this->schedule;
    }

    private function isThisDayGood(\AbedMahfouz\Scheduling\SectionCourse $sc, $day) {
        foreach ($this->sessionIDs as $session){
            $key = Helper::makeScheduleKey($sc->section->section_id, $day, $session);

            // if there is schedule check for course
            if (isset($this->schedule[$key])) {
                if ($this->schedule[$key]['sectionCourse'] === $sc) {
                    return false;
                }
            }
        }
        return true;
    }

    private function tryAssignProf(\AbedMahfouz\Scheduling\SectionCourse $sc, $profIDs = [], $day, $session, $returnProfIfSuccess = false) {
        $key = Helper::makeScheduleKey($sc->section->section_id, $day, $session);

        $added = false;
        shuffle($profIDs);
        foreach ($profIDs as $randomProf) {
            $randomProf = $profIDs[array_rand($profIDs)];

            // skip when prof free
            if ($this->profsSchedule[$randomProf][$day][$session] != null) {
                continue;
            }

            // insert when all good
            $this->profsSchedule[$randomProf][$day][$session] = (string) $sc;
            $this->schedule[$key] = [
                'sectionCourse' => $sc,
                'prof_id' => $randomProf,
            ];

            $added = true;
            break;
        }

        if ($added && $returnProfIfSuccess) {
            return false;
        }
        return $added;
    }

    private function tryAddOneHour(\AbedMahfouz\Scheduling\SectionCourse $sc, $profIDs = [], $maxTry) {
        for ($i = 0; $i < $maxTry; $i++) {

            $randomDay = $this->dayIDs[array_rand($this->dayIDs)];
            $randomSession = $sc->course_priority == 1 ? 1 : $this->sessionIDs[array_rand($this->sessionIDs)];

            if (!$this->isThisDayGood($sc, $randomDay)) {
                continue;
            }

            $key = Helper::makeScheduleKey($sc->section->section_id, $randomDay, $randomSession);

            // skip when schedule filled
            if (isset($this->schedule[$key])) {
                continue;
            }

            // last, try add prof
            if ($this->tryAssignProf($sc, $profIDs, $randomDay, $randomSession)) {
                return true;
            }
        }

        return false;
    }

    private function tryAddTwoHour(\AbedMahfouz\Scheduling\SectionCourse $sc, $profIDs = [], $maxTry) {
        for ($i = 0; $i < $maxTry; $i++) {
            $randomDay = $this->dayIDs[array_rand($this->dayIDs)];
            if (!$this->isThisDayGood($sc, $randomDay)) {
                continue;
            }

            // random session
            if($sc->course_priority == 1) {
                $randomSession = [$this->sessionIDs[0], $this->sessionIDs[1]];
            } else {
                // first session must be not last session :-)
                $firstSessionKey = mt_rand(0, count($this->sessionIDs) - 2);
                $randomSession = [
                    $this->sessionIDs[$firstSessionKey],
                    $this->sessionIDs[$firstSessionKey + 1]
                ];
            }

            $key1 = Helper::makeScheduleKey($sc->section->section_id, $randomDay, $randomSession[0]);
            $key2 = Helper::makeScheduleKey($sc->section->section_id, $randomDay, $randomSession[1]);


            // skip when schedule filled
            if (isset($this->schedule[$key1]) || isset($this->schedule[$key2])) {
                continue;
            }

            // try add prof at first session
            $firstProf = $this->tryAssignProf($sc, $profIDs, $randomDay, $randomSession[0], true);
            if ($firstProf == false) {
                continue;
            }

            // last, try add prof at second session, if false then undo first session :-)
            if ($this->tryAssignProf($sc, $profIDs, $randomDay, $randomSession[1])) {
                return true;
            } else {
                $this->profsSchedule[$firstProf][$randomDay][$randomSession[0]] = null;
                unset($this->schedule[$key1]);
                continue;
            }
        }

        return false;
    }


    private function clearSchedule() {
        $this->schedule = [];
    }

    /**
     * generate profes schedule, to make sure schedule not conflict
     * null when he/she is free, add section ID if filled
     *
     * [
     *     prof_id => [
     *         day1 => [
     *             sess1 => <SECTION_ID>, sess2 => null, sess3 => null, ... sess7 => null,
     *         ],
     *         .
     *         .
     *         .
     *         day7 => [
     *             sess1 => null, sess2 => null, sess3 => null, ... sess7 => null,
     *         ],
     *     ],
     *
     *     prof_idN => [
     *         day1 => [
     *             sess1 => null, sess2 => null, sess3 => null, ... sess7 => null,
     *         ],
     *         .
     *         .
     *         .
     *         day7 => [
     *             sess1 => null, sess2 => null, sess3 => null, ... sess7 => null,
     *         ],
     *     ]
      *]
     */
    private function resetProfSchedule() {
        $profsSchedule = [];
        foreach ($this->profIDs as $profId) {
            $profsSchedule[$profId] = [];
            foreach (array_keys($this->daysSessions) as $day) {
                $profsSchedule[$profId][$day] = [];
                foreach ($this->daysSessions[$day] as $session) {
                    $profsSchedule[$profId][$day][$session] = null;
                }
            }
        }

        $this->profsSchedule = $profsSchedule;
        return $this->profsSchedule;
    }


    /**
     * generate day IDs & session IDs for easier check and random time,
     * $this->dayIDs = [day1, day2, ..., day7]
     * $this->sessionIDs = [sess1, sess2, ..., sess7]
     */
    private function resetDaySessionIDs() {
        $this->dayIDs = array_keys($this->daysSessions);
        $this->sessionIDs = $this->daysSessions[$this->dayIDs[0]];
    }

    private function clearMessages(){
        $this->warnings = [];
        $this->errors = [];
    }





}
