<?php
namespace AbedMahfouz\Scheduling;

class Section {
    public $class_id;
    public $class_name;
    public $section_id;
    public $section_name;

    public function __construct($class_id, $class_name, $section_id, $section_name) {
        $this->class_id = intval($class_id);
        $this->class_name = $class_name;
        $this->section_id = intval($section_id);
        $this->section_name = $section_name;
    }

    /**
     * format:
     *   - #1-1 seventh - A
     *   - #<class_id>-<section_id> <class_name> - <section_name>
     *
     * @return string
     */
    public function __toString() {
        return '#'.$this->class_id.'-'.$this->section_id.' '.$this->class_name.' - '.$this->section_name;
    }

}
