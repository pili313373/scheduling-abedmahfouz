<?php
namespace AbedMahfouz\Scheduling;

class SectionCourse {

    public $section;
    public $course_id;
    public $course_name;
    public $course_hours;
    public $course_priority;

    /**
     * @param  \AbedMahfouz\Scheduling\Section  $section
     * @param  int      $course_id
     * @param  string   $course_name
     * @param  int      $course_hours
     * @param  int      $course_priority
     * @return void
     */
    public function __construct(\AbedMahfouz\Scheduling\Section $section, $course_id, $course_name, $course_hours, $course_priority = 0) {
        $this->section = $section;
        $this->course_id = intval($course_id);
        $this->course_name = $course_name;
        $this->course_hours = intval($course_hours);
        $this->course_priority = intval($course_priority);
    }

    /**
     * format:
     *   - <print section> / #math
     *   - <print section> / <course_name>
     *   - #<class_id>-<section_id> <class_name> - <section_name>
     *
     * @return string
     */
    public function __toString() {
        return $this->section.' / '.$this->course_name;
    }

}
