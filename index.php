<?php
set_time_limit(0);

require __DIR__.'/vendor/autoload.php';

use Carbon\Carbon;
use AbedMahfouz\Scheduling\Section;
use AbedMahfouz\Scheduling\SectionCourse;
use AbedMahfouz\Scheduling\ProfRelation;
use AbedMahfouz\Scheduling\Generator;
use AbedMahfouz\Scheduling\Helper;

// config
$teach_year_id = 0;
$maxSessionPerDay = 2;

// Sql class, contain all SQL needed
require __DIR__.'/SQL.php';

// memory database (arrays)
$sections = [];
$sectionCourses = [];
$profRelations = [];
$profIDs = [];
$daysSessions = [];
$days = [];
$generatedSchedule = [];

// db config
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '1');
define('DB_NAME', 'scheduling');

// db connection
$db = new Mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if ($db->connect_errno) {
    echo "Errno: " . $db->connect_errno . "\n";
    echo "Error: " . $db->connect_error . "\n";
    exit;
}
if (!$db->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $db->error);
    exit;
}

// load sections
$q = $db->query(SQL::getAllSection());
while ($row = $q->fetch_assoc()) {
    $sections[$row['section_id']] = new Section(
                                        $row['class_id'],
                                        $row['class_name'],
                                        $row['section_id'],
                                        $row['section_name']
                                    );
}

// load course-section
$q = $db->query(SQL::getAllCoursePerSection());
while ($row = $q->fetch_assoc()) {
    $key = Helper::makeSectionCourseKey($row['section_id'], $row['course_id']);
    $sectionCourses[$key] = new SectionCourse(
                                $sections[$row['section_id']],
                                $row['course_id'],
                                $row['course_name'],
                                $row['course_hours'],
                                $row['course_priority']
                            );

}

// load prof relations
$q = $db->query(SQL::getProfSectionCourseRelations($teach_year_id));
while ($row = $q->fetch_assoc()) {
    $key = Helper::makeSectionCourseKey($row['section_id'], $row['course_id']);
    $profRelations[] = new ProfRelation(
                            $sectionCourses[$key],
                            $row['prof_id']
                        );
    // $profIDs[$row['prof_id']] = $row['prof_id'];
    $profIDs[] = $row['prof_id'];
}

// load days sessions
$qDays = $db->query(SQL::getAllDay());
while ($day = $qDays->fetch_assoc()) {
    $days[$day['id']] = $day['name'];
    $tmp = [];

    $qSessions = $db->query(SQL::getAllSession());
    while ($session = $qSessions->fetch_assoc()) {
        $tmp[] = intval($session['id']);
    }

    $daysSessions[intval($day['id'])] = $tmp;
}

// call generator
$generator = new Generator($sections, $sectionCourses, $profRelations, $profIDs, $daysSessions, $maxSessionPerDay);
$generatedSchedule = $generator->generate();
if (count($generator->errors) > 0) {
    var_dump($generator->errors);
    exit;
}

// var_dump($this->warnings, $this->errors);



// print all
require __DIR__.'/print_timetable.php';
print_timetable($sections, $sectionCourses, $profRelations, $daysSessions, $generatedSchedule, $days);
