<?php 

class SQL {
    
    public static function getAllDay() {
        return "
            SELECT
                d.`ID` as `id`,
                d.`jour` as `name`
            FROM `jour_semain` d
            WHERE d.`status` = 1
        ";
    }
    
    public static function getAllSession() {
        return "
            SELECT
                s.`ID` as `id`
            FROM `session` s
        ";
    }
    
    public static function getAllSection() {
        return "
            SELECT
                c.`ID` as `class_id`,
                c.`classe` as `class_name`,
                s.`ID` as `section_id`,
                st.`title` as `section_name`
                
            FROM `section` s
            JOIN `section_title` as st ON s.`section_title_id` = st.`ID`
            JOIN `classe` c  ON s.`class_id` = c.`ID`
            ORDER BY 
                c.`ID` ASC,
                st.`ID` ASC
        ";
    }
    
    public static function getAllCoursePerSection() {
        return "
            SELECT
                s.`ID` as `section_id`,
                concat(st.`title`, '-', c.`classe`) as `class_section_name`,
                m.`ID` as `course_id`,
                m.`matiere` as `course_name`,
                mpivot.`nbr_heure` as `course_hours`,
                mpivot.`priority` as `course_priority`
                
            FROM `section` s
            JOIN `section_title` as st ON s.`section_title_id` = st.`ID`
            JOIN `classe` c  ON s.`class_id` = c.`ID`

            JOIN `has_matiere` mpivot ON c.`ID` = mpivot.`class_id`
            JOIN `matiere` m ON mpivot.`matiere_id`  = m.`ID`

            ORDER BY 
                c.`ID` ASC,
                st.`ID` ASC,
                m.`ID` ASC
        ";
    }
    
    public static function getProfSectionCourseRelations($year_id = 0) {
        return "
            SELECT DISTINCT 
                s.`ID` as `section_id`,
                concat(st.`title`, '-', c.`classe`) as `class_section_name`,
                m.`ID` as `course_id`,
                m.`matiere` as `course_name`,
                teach.`Prof_id` as `prof_id`
                
            FROM `teach` teach
            JOIN `section` s on teach.`section_id` = s.`ID`
            JOIN `section_title` as st ON s.`section_title_id` = st.`ID`
            JOIN `classe` c  ON s.`class_id` = c.`ID`
            JOIN `matiere` m ON teach.`matiere_id`  = m.`ID`
            
            -- just to make sure, we check to this table again `has_matiere`
            JOIN `has_matiere` mpivot 
                ON c.`ID` = mpivot.`class_id`
                AND teach.`matiere_id`

            WHERE teach.`year_id` = '{$year_id}'

            ORDER BY 
                s.`ID` ASC,
                m.`ID` ASC,
                teach.`Prof_id` ASC
        ";
    }
}