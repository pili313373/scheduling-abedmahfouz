<?php
require __DIR__.'/vendor/autoload.php';

define('NL', '<br />'.PHP_EOL);

use Carbon\Carbon;
use AbedMahfouz\Scheduling\Section;
use AbedMahfouz\Scheduling\SectionCourse;
use AbedMahfouz\Scheduling\ProfRelation;
use AbedMahfouz\Scheduling\Helper;

/**
 * a helper function for print all timetable generated in HTML
 * @param  array of \AbedMahfouz\Scheduling\Section  $sections
 * @param  array of \AbedMahfouz\Scheduling\SectionCourse  $sectionCourses
 * @param  array of \AbedMahfouz\Scheduling\ProfRelation  $profRelations
 * @param  array $daysSessions
 * @param  array $schedule
 * @param  array $days
 * @return void
 */
function print_timetable($sections = [], $sectionCourses = [], $profRelations = [], $daysSessions = [], $schedule = [], $days = []) {
    ?><!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
    </head>
    <body>
    <div style="margin:auto; width:960px;">
    <?php foreach ($sections as $section): ?>
        <h3><?php echo $section; ?></h3>

        <strong>- Available courses:</strong>
        <br/>
        <br/>
        <table class="pure-table">
            <thead>
                <tr>
                    <th>Course Name</th>
                    <th>Hours</th>
                    <th>Prority</th>
                    <th>Prof ID</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($sectionCourses as $sc):
                        if ($sc->section !== $section) continue;
                        $profs = [];
                        foreach($profRelations as $pr) {
                            if ($pr->sectionCourse === $sc) {
                                $profs[] = $pr->prof_id;
                            }
                        }
                        $prof_id = count($profs) > 0 ? join(', ', $profs) : '-';
                ?>
                <tr>
                    <td><?php echo $sc->course_name; ?></td>
                    <td><?php echo $sc->course_hours; ?></td>
                    <td><?php echo $sc->course_priority; ?></td>
                    <td><?php echo $prof_id; ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <br/>
        <strong>- Schedule:</strong>
        <br/>
        <br/>
        <table class="pure-table">
            <thead>
                <tr>
                    <th>Session</th>
                    <?php foreach(array_keys($daysSessions) as $day): ?>
                    <th><?php echo ucwords($days[$day]); ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach (reset($daysSessions) as $session_id): ?>
                <tr>
                    <td><?php echo $session_id; ?></td>
                    <?php foreach ($daysSessions as $day => $v): ?>
                    <?php
                        $key = Helper::makeScheduleKey($section->section_id, $day, $session_id);
                        // null -> '-'
                        $courseProf = isset($schedule[$key]) ? $schedule[$key] : null;
                        $courseProf = $courseProf == null ? '-' : $courseProf['sectionCourse']->course_name.' / '.$courseProf['prof_id'];
                    ?>
                    <td><?php echo $courseProf; ?></td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <br/>
        <hr/>
        <?php
    endforeach;
    ?>
    </div>
    </body>
    </html>
    <?php
}
